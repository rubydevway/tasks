def replacement(a)
	b, c, = a.index(a.max), a.index(a.min)
	a[b], a[c] = a[c], a[b]
	a
end

#p replacement [2,3,4,5]

def more_less(a, num, more = [])

	a.each do |e|
	 	c = e > num ? true : false 
	 	more << c		
	end 

	!more.include?(false)
end

#p more_less [0,3,4,51], 1

def local_mins(a, count = 0)
	a.each_cons(3){ |e| count += 1 if e[1] < e[0] && e[1] < e[2] }
	count
end	

#p local_mins [2,1,2,3,4,3,5,4,5] #2 local mins

def caesar_incode(str, shift, a = ('a'..'z').to_a)
	rezult = ''

	str.each_char do |c|
		
		if a.include?(c) 
			shift_index = a.index(c) + shift if a.index(c) + shift < a.length
			shift_index = ((a.index(c) + shift)%a.length) if a.index(c) + shift >= a.length
			rezult += a[shift_index]
		else 
			rezult += c 
		end	
	end	
		
	rezult	
end

p caesar_incode('to be, or not to be', 27)













